const dgram = require("dgram");
const fs = require("fs");

const UDP_MAX_PACKET_SIZE = 65507;
const CURRENT_PACKET_BYTES = 8;
const TOTAL_PACKETS_BYTES = 8;
const FILE_SHA1_BYTES = 20;
const QUEUE_BYTES = 8;
const METADATA_PACKETS_AMOUNT = 1;

module.exports = {
  overUDP: streamFileOverUDP
};

function streamFileOverUDP({
  host,
  port,
  filepath,
  filesha1,
  destination,
  maxPacketSize = UDP_MAX_PACKET_SIZE
}) {
  return new Promise((resolve, reject) => {
    const client = dgram.createSocket("udp4");
    const fileSize = fs.statSync(filepath).size;
    const PACKET_CONTENT_SIZE =
      maxPacketSize -
      CURRENT_PACKET_BYTES -
      TOTAL_PACKETS_BYTES -
      FILE_SHA1_BYTES -
      QUEUE_BYTES;

    const chuncksCount = METADATA_PACKETS_AMOUNT + Math.ceil(fileSize / PACKET_CONTENT_SIZE);
    console.info(
      "Sending file `%s`.\n\tSize: %s.\n\tDestination: %s:%d.\n\tSending: %d packets.",
      filepath,
      fileSize.toLocaleString(),
      host,
      port,
      chuncksCount
    );

    const metadata_packet = Buffer.from(destination);

    client.send(metadata_packet, 0, metadata_packet.length, port, host);
    const hrstart = process.hrtime();
    const fStream = fs.createReadStream(filepath, {
      highWaterMark: 128 * 1024
    });

    let i = 1;

    fStream.on("data", k => {
      const buffer = Buffer.concat(
        [
          uint32BufferOf(i++),
          uint32BufferOf(chuncksCount),
          Buffer.from(filesha1),
          k
        ],
        maxPacketSize
      );
      client.send(buffer, 0, buffer.length, port, host);
    });

    fStream.on("error", reject);
    client.on("error", reject);

    fStream.on("end", () => {
      const [, durationInNano] = process.hrtime(hrstart);
      console.info(
        "Execution time: \n\t%dms total.\n\t%s total bytes.\n\t%dms per packet.\n\t%d packets total",
        durationInNano / 1000000,
        fileSize.toLocaleString(),
        durationInNano / 1000000 / chuncksCount,
        chuncksCount
      );
      client.on("end", () => {
        resolve();
        fStream.close();
        client.close();
      });
    });
  });
}

function uint32BufferOf(number) {
  const buf = Buffer.allocUnsafe(4);

  buf.writeUInt32BE(number, 0);

  return buf;
}
